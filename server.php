<?php
session_start();

$username = "";
$email = "";
$error = array();
  //connect to  Database
  $db = mysqli_connect('localhost','root','','register') or die($db);

  //$db = mysqli_connect('localhost', 'root', '', 'demoreg') or die($db);
  //if the register button is clicked
  if(isset($_POST['register'])){
    $username = mysqli_real_escape_string($db, $_POST['username']);
    $email = mysqli_real_escape_string($db, $_POST['email']);
    $password1 = mysqli_real_escape_string($db, $_POST['password1']);
    $password2 = mysqli_real_escape_string($db, $_POST['password2']);

    //ensure that form field are filled properly
    if(empty($username)){
      array_push($error,"Username is required");
    }
    if(empty($email)){
      array_push($error,"Email is required");
    }
    if(empty($password1)){
      array_push($error,"Password is required");
    }
    if($password1 != $password2){
      array_push($error,"The two password do not match !");
    }

    //if there are no error, data save to SQLiteDatabase
    if(count($error) == 0){
      $password = md5($password1); //md5 use to encrypt password before store in dtbse
      $sql = "INSERT INTO users(username, email, password) VALUES ('$username', '$email', $password)";
      mysqli_query($db,$sql);
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in.";
      header('location: index.php'); //redirect to homepage
    }
  }
//log user in from login dtbse
if(isset($_POST['login'])){
  $username = mysqli_real_escape_string($db, $_POST['username']);
  $password = mysqli_real_escape_string($db, $_POST['password']);
  //ensure that form field are filled properly
  if(empty($username)){
    array_push($error,"Username is required");
  }
  if(empty($password)){
    array_push($error,"Password is required");
  }
  if(count($error) == 0){
    $password = md5($password);
    $query = "SELECT * FROM `users` WHERE username = '$username' AND password = '$password'";
    $result = mysqli_query($db, $query);
    if(mysqli_num_rows($result) ==  0){
      //log user in
      $_SESSION['username'] = $username;
      $_SESSION['success'] = "You are now logged in.";
      header('location: index.php'); //redirect to homepage
    }else{
      array_push($error, "Wrong username/password combination");
    }
  }

}
//Logout
if(isset($_GET['logout'])){
  session_destroy();
  unset($_SESSION['username']);
  header('location: login.php');
}
 ?>
