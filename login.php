<?php
include('server.php');
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User Login</title>
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </head>
  <body>

    <div class="header">
      <h2>LOGIN</h2>
    </div>
  <form class="" action="login.php" method="post">
    <!-- display validation errors -->
    <?php
      include('error.php');
     ?>
    <div class="input-group">
      <label for="">Username</label>
      <input type="text" name="username">
    </div>
    <div class="input-group">
      <label for="">Password</label>
      <input type="password" name="password">
    </div>
    <div>
      <button type="submit" name="login" class="btn btn-success">Login</button>
    </div>
    <br>
    <p id = "member">
      Not a member? <a href="register.php">Sign up</a>
    </p>
  </form>
  </body>
</html>
