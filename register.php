<?php
  include('server.php');
 ?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>User registration</title>
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  </head>
  <body>

    <div class="header">
      <h2>REGISTER</h2>
    </div>
  <form class="" action="register.php" method="post">
    <!-- display validation errors -->
    <?php
      include('error.php');
     ?>
    <div class="input-group">
      <label for="">Username</label>
      <input type="text" name="username">
    </div>
    <div class="input-group">
      <label for="">Email</label>
      <input type="email" name="email">
    </div>
    <div class="input-group">
      <label for="">Password</label>
      <input type="password" name="password1">
    </div>
    <div class="input-group">
      <label for="">Confirm Password</label>
      <input type="password" name="password2">
    </div>
    <div>
      <button type="submit" name="register" class="btn btn-success">Register</button>
    </div>
    <br>
    <p id = "member">
      Already a member? <a href="login.php">Login</a>
    </p>
  </form>
  </body>
</html>
